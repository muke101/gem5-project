
# Gem5 Aliasing Project by Rohan Gandhi

- To recursively clone gem5 submodule this project: use ```git clone --recurse-submodule ssh://git@gitlab-uk.rnd.huawei.com:2222/r50028931$
- To recursively pull this repo, use git pull --recurse-submodules origin


# Folders

```gem5``` : the latest version of gem5 --> it is a submodule of the repository gem5 on my gitlab: ```ssh://git@gitlab-uk.rnd.huawei.com:2222/r50028931/gem5.git``` ... so anything you push to that will change that repository...
- if there are any muck ups ```CounterAliasingV100``` is the same as ```master``` in my gem5 repository

```tips.md``` : my internship notes + gem5 tips

```testscipts``` : contains various types of test scripts to run various testing structures for gem5 
- **NOTE:** need to change folders if they have changed etc when using these test scripts...

```clean_bins``` : contains the binaries that you want to run

```results``` : should contain the results of the statistics outputted when the test script is run
- **NOTE:** this gets deleted when you re-run a test script (change if you want)

```microbenchmarks``` : contains the final benchmarks used to test my implementation of gem5.

```dataextraction``` : contains files to extra the statistics to excel
