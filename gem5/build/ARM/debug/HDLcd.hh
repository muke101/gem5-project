/**
 * DO NOT EDIT THIS FILE!
 * File automatically generated by
 *   build_tools/debugflaghh.py:127
 */

#ifndef __DEBUG_HDLcd_HH__
#define __DEBUG_HDLcd_HH__

#include "base/compiler.hh" // For namespace deprecation
#include "base/debug.hh"
namespace gem5
{

GEM5_DEPRECATED_NAMESPACE(Debug, debug);
namespace debug
{

namespace unions
{
inline union HDLcd
{
    ~HDLcd() {}
    SimpleFlag HDLcd = {
        "HDLcd", "", false
    };
} HDLcd;
} // namespace unions

inline constexpr const auto& HDLcd = 
    ::gem5::debug::unions::HDLcd.HDLcd;

} // namespace debug
} // namespace gem5

#endif // __DEBUG_HDLcd_HH__
