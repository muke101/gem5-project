/**
 * DO NOT EDIT THIS FILE!
 * File automatically generated by
 *   build_tools/debugflaghh.py:127
 */

#ifndef __DEBUG_UFSHostDevice_HH__
#define __DEBUG_UFSHostDevice_HH__

#include "base/compiler.hh" // For namespace deprecation
#include "base/debug.hh"
namespace gem5
{

GEM5_DEPRECATED_NAMESPACE(Debug, debug);
namespace debug
{

namespace unions
{
inline union UFSHostDevice
{
    ~UFSHostDevice() {}
    SimpleFlag UFSHostDevice = {
        "UFSHostDevice", "", false
    };
} UFSHostDevice;
} // namespace unions

inline constexpr const auto& UFSHostDevice = 
    ::gem5::debug::unions::UFSHostDevice.UFSHostDevice;

} // namespace debug
} // namespace gem5

#endif // __DEBUG_UFSHostDevice_HH__
