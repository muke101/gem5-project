/**
 * DO NOT EDIT THIS FILE!
 * File automatically generated by
 *   build_tools/debugflaghh.py:127
 */

#ifndef __DEBUG_staticInstruction_HH__
#define __DEBUG_staticInstruction_HH__

#include "base/compiler.hh" // For namespace deprecation
#include "base/debug.hh"
namespace gem5
{

GEM5_DEPRECATED_NAMESPACE(Debug, debug);
namespace debug
{

namespace unions
{
inline union staticInstruction
{
    ~staticInstruction() {}
    SimpleFlag staticInstruction = {
        "staticInstruction", "", false
    };
} staticInstruction;
} // namespace unions

inline constexpr const auto& staticInstruction = 
    ::gem5::debug::unions::staticInstruction.staticInstruction;

} // namespace debug
} // namespace gem5

#endif // __DEBUG_staticInstruction_HH__
