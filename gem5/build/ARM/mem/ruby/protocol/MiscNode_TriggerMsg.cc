/**
 * DO NOT EDIT THIS FILE!
 * File automatically generated by
 *   /home/muke/Programming/Huawei/gem5-aliasing/gem5/src/mem/slicc/symbols/Type.py:453
 */

#include <iostream>
#include <memory>

#include "mem/ruby/protocol/MiscNode_TriggerMsg.hh"
#include "mem/ruby/system/RubySystem.hh"

namespace gem5
{

namespace ruby
{

/** \brief Print the state of this object */
void
MiscNode_TriggerMsg::print(std::ostream& out) const
{
    out << "[MiscNode_TriggerMsg: ";
    out << "txnId = " << printAddress(m_txnId) << " ";
    out << "from_hazard = " << m_from_hazard << " ";
    out << "]";
}
bool
MiscNode_TriggerMsg::functionalRead(Packet* param_pkt)
{
return (false);

}
bool
MiscNode_TriggerMsg::functionalRead(Packet* param_pkt, WriteMask& param_mask)
{
return (false);

}
bool
MiscNode_TriggerMsg::functionalWrite(Packet* param_pkt)
{
return (false);

}
} // namespace ruby
} // namespace gem5
