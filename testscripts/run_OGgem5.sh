#! /bin/bash

echo "Running all benchmarks"
echo "#################################"
echo "Clearing stats files in ~/results"
echo ""
echo "#################################"
rm -rf ~/results
mkdir results

for i in ~/clean_bins/*; do 
	b=$(basename -s .c ${i});
	echo "Running benchmark: $b "
	echo "#####################"

    echo "Running processed file!"
    echo "##########################"
    file ~/clean_bins/$b 
    echo " "
    echo "Building now" 
    echo "#####################"
    ./originalgem5/gem5/build/ARM/gem5.opt --debug-flags=ViolationEvent -d ~/originalgem5/gem5/m5out originalgem5/gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches -c ~/clean_bins/$b --options "10 0.25 0.5 0" > logfolder/${b}_allalias.log
    echo " "
    echo "Build finished"
    echo "#####################"
    echo " "
    echo "Copied stats file to ~/results"
    echo "#####################"
    cp ~/originalgem5/gem5/m5out/stats.txt ~/results/
    mv ~/results/stats.txt ~/results/${b}_allalias_stats.txt

    echo "Running processed file!"
    echo "##########################"
    file ~/clean_bins/$b 
    echo " "
    echo "Building now" 
    echo "#####################"
    ./originalgem5/gem5/build/ARM/gem5.opt --debug-flags=ViolationEvent -d ~/originalgem5/gem5/m5out originalgem5gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches -c ~/clean_bins/$b --options "10 0.25 0.5 1" > logfolder/${b}_everyotheralias.log
    echo " "
    echo "Build finished"
    echo "#####################"
    echo " "
    echo "Copied stats file to ~/results"
    echo "#####################"
    cp ~/originalgem5/gem5/m5out/stats.txt ~/results/
    mv ~/results/stats.txt ~/results/${b}_everyotheralias_stats.txt

    echo "Running processed file!"
    echo "##########################"
    file ~/clean_bins/$b 
    echo " "
    echo "Building now" 
    echo "#####################"
    ./originalgem5/gem5/build/ARM/gem5.opt --debug-flags=ViolationEvent -d ~/originalgem5/gem5/m5out originalgem5/gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches -c ~/clean_bins/$b --options "10 0.25 0.5 2" > logfolder/${b}_neveralias.log
    echo " "
    echo "Build finished"
    echo "#####################"
    echo " "
    echo "Copied stats file to ~/results"
    echo "#####################"
    cp ~/originalgem5/gem5/m5out/stats.txt ~/results/
    mv ~/results/stats.txt ~/results/${b}_neveralias_stats.txt
done
