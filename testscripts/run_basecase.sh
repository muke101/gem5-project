#! /bin/bash


echo "Running all benchmarks"
echo "#################################"
echo "Clearing stats files in ~/results"
echo ""
echo "#################################"
rm -rf ~/results
mkdir results

for i in ~/clean_bins/*; do 
	b=$(basename -s .c ${i});
	echo "Running benchmark: $b "
	echo "#####################"
	file ~/clean_bins/$b
	echo " "
	echo "Building now" 
	echo "#####################"
	./gem5/build/ARM/gem5.opt -d ~/gem5/m5out gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches --options "i 1" -c clean_bins/$b
	echo " "
	echo "Build finished"
	echo "#####################"
	echo " "
	echo "Copied stats file to ~/results"
	echo "#####################"
	cp ~/gem5/m5out/stats.txt ~/results/
	mv ~/results/stats.txt ~/results/${b}_stats.txt
	

done
