#! /bin/bash


echo "Running all benchmarks"
echo "#################################"
echo "Clearing stats files in ~/results"
echo ""
echo "#################################"
rm -rf ~/results
mkdir results

for i in ~/clean_bins/*; do 
	b=$(basename -s .c ${i});
	echo "Running benchmark: $b "
	echo "#####################"
	cp ${i} ~/proc_bins
	python3 patcher.py ~/proc_bins/$b
	echo " "	
	echo "Processed binary and moving to ~/proc_bins"
	echo "#####################"
	file ~/proc_bins/$b
	echo " "
	echo "Building now" 
	echo "#####################"
	./gem5/build/ARM/gem5.opt --debug-flags=Counter -d ~/gem5/m5out gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches -c ~/proc_bins/$b > logfolder/${b}
	echo " "
	echo "Build finished"
	echo "#####################"
	echo " "
	echo "Copied stats file to ~/results"
	echo "#####################"
	cp ~/gem5/m5out/stats.txt ~/results/
	mv ~/results/stats.txt ~/results/${b}_stats.txt
	

done
