#! /bin/bash

echo "Running all benchmarks"
echo "#################################"
echo "Clearing stats files in ~/results"
echo ""
echo "#################################"
rm -rf ~/results
mkdir results

for i in ~/clean_bins/*; do 
	b=$(basename -s .c ${i});
	echo "Running benchmark $b with parameters: length of array [30], float 1 [0.5] and float 2[0.25]"
	echo "#####################"
    ## first with all iterations having an alias:
    echo "Running file $b!"
    echo "##########################"
    file ~/clean_bins/$b 
    echo "Updating indices file!"
    echo "##########################"
    python3 indecies-script.py 0 10 +1
    cat indecies
    echo " "
    echo "Building now" 
    echo "#####################"
    ./gem5/build/ARM/gem5.opt -d ~/gem5/m5out gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches -c ~/clean_bins/$b --options "30 0.5 0.25"
    echo " "
    echo "Build finished"
    echo "#####################"
    echo " "
    echo "Copied stats file to ~/results"
    echo "#####################"
    cp ~/gem5/m5out/stats.txt ~/results/
    mv ~/results/stats.txt ~/results/${b}__allalias_stats.txt

    ## then only first iteration with an alias:
    echo "Running file $b!"
    echo "##########################"
    file ~/clean_bins/$b 
    echo "Updating indices file!"
    echo "##########################"
    python3 indecies-script.py 0 1 +1
    cat indecies
    echo " "
    echo "Building now" 
    echo "#####################"
    ./gem5/build/ARM/gem5.opt -d ~/gem5/m5out gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches -c ~/clean_bins/$b --options "30 0.5 0.25"
    echo " "
    echo "Build finished"
    echo "#####################"
    echo " "
    echo "Copied stats file to ~/results"
    echo "#####################"
    cp ~/gem5/m5out/stats.txt ~/results/
    mv ~/results/stats.txt ~/results/${b}_1alias_stats.txt

    ## then every other iteration with an alias:
    echo "Running file $b!"
    echo "##########################"
    file ~/clean_bins/$b 
    echo "Updating indices file!"
    echo "##########################"
    python3 indecies-script.py 0 5 +2
    cat indecies
    echo " "
    echo "Building now" 
    echo "#####################"
    ./gem5/build/ARM/gem5.opt -d ~/gem5/m5out gem5/configs/example/se.py --cpu-type=DerivO3CPU --caches -c ~/clean_bins/$b --options "30 0.5 0.25"
    echo " "
    echo "Build finished"
    echo "#####################"
    echo " "
    echo "Copied stats file to ~/results"
    echo "#####################"
    cp ~/gem5/m5out/stats.txt ~/results/
    mv ~/results/stats.txt ~/results/${b}_everyotheralias_stats.txt
done

echo "Extracting file from ~/results"
echo "#####################"
python3 extract_data_toexcel.py